<!doctype html>
<html>
    <head>
        <title>test</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <style>
            body{
                font-family: "Nunito";
            }
        </style>
    </head>

    <body>
    <div>
        @if (Route::has('login'))

            <div class="px-6 py-4 d-flex justify-content-between">
                <div>
                    <h4>RMS</h4>
                </div>
                <div class="mr-2">
                @auth
                    <a href="{{ url('/home') }}" class="text-sm text-gray-700 underline">Home</a>
                @else
                        @include('popup_forms.loginForm')

                    @if (Route::has('register'))
                        @include('popup_forms.regesterForm')
                    @endif
                @endif
                </div>
            </div>
        @endif

    </div>
    </body>
</html>
